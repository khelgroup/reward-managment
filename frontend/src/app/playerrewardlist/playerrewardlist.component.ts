import {Component, OnInit} from '@angular/core';
import {NotificationService} from "../services/notification.service";
import {ConfigurationService} from "../services/configuration.service";

@Component({
    selector: 'app-playerrewardlist',
    templateUrl: './playerrewardlist.component.html',
    styleUrls: ['./playerrewardlist.component.scss']
})
export class PlayerrewardlistComponent implements OnInit {


    rows: any = [];
    config: any = [];
    totalSize;
    currentPage = 0;
    pageSize = 5;
    display = 'none';
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '20',
            value: 20
        },
        {
            key: '50',
            value: 50
        }
    ];
    configurationId = [];

    constructor(private configurationService: ConfigurationService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.getPlayerRewardHistory(this.currentPage, this.pageSize);
    }

    getPlayerRewardHistory(pageNumber, pageSize) {
        let userId = localStorage.getItem('userId')
        this.configurationService.getPlayerRewardHistory(pageNumber, pageSize, userId)
            .subscribe((response) => {
                    if (!response.error) {
                        console.log('res', response);
                        this.rows = response.data.results;
                        this.totalSize = response.data.count;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getPlayerRewardHistory(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getPlayerRewardHistory(e.offset, e.pageSize);
    }
}
