import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../app/services/notification.service';
import {ConfigurationService} from '../services/configuration.service';

import * as moment from 'moment';

@Component({
    selector: 'app-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

    receiveData: any = {};
    oldData: any = {};
    rewardList: any = {};
    userClaimRewards: any = [];

    constructor(private notificationService: NotificationService,
                private configurationService: ConfigurationService) {
    }

    ngOnInit() {
        this.getPlayerInfo();
    }

    /*getUserRewardHistory() {
        let userId = localStorage.getItem('userId');
        this.configurationService.getAdminRewards(userId)
            .subscribe((userRewards) => {
                    if (!userRewards.error) {
                        this.userClaimRewards = userRewards.data.results;
                        console.log('userClaimRewards', this.userClaimRewards);
                    } else {
                        this.notificationService.showNotification(userRewards.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    getFullRewardList() {
        this.configurationService.getallRewards()
            .subscribe((rewardList) => {
                    if (!rewardList.error) {
                        this.rewardList = rewardList.data.results;
                        this.getUserRewardHistory()
                    } else {
                        this.notificationService.showNotification(rewardList.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }*/

    getPlayerInfo() {
        let userId = localStorage.getItem('userId')
        this.configurationService.getPlayerInfo(userId)
            .subscribe((response) => {
                    if (!response.error) {
                        this.receiveData = response.data;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }

                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    UpdateProfile() {
        let userId = localStorage.getItem('userId');
        this.receiveData['playerId'] = userId;
        this.configurationService.updatePlayerProfile(this.receiveData)
            .subscribe((res) => {
                    if (!res.error) {
                        this.notificationService.showNotification(res.message, 'success');
                        this.getPlayerInfo()
                    } else {
                        this.notificationService.showNotification(res.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
        /*this.oldData = {
            lastName: ""
        }
        let keys = Object.keys(this.oldData);
        let ary = [];
        let readyToClaimReward = [];
        keys.forEach((x) => {
            // console.log('Key--Old--New', x, this.oldData[x], this.receiveData[x]);
            if (this.oldData[x] !== this.receiveData[x]) {
                ary.push(x);
            }
        })
        console.log('aryyyyyyyyyyy', ary);
        console.log('aryyyyyyyyyyy', this.rewardList);
        if (ary.length > 0) {
            (ary).forEach((x) => {
                (this.rewardList).map(function (y, i) {
                    if (y["rewardName"] === x || y['keywords'].indexOf(x) !== -1) {
                        readyToClaimReward.push(y);
                    }
                })
            })
            console.log('readyToClaimReward', readyToClaimReward);
            if (Object.keys(this.userClaimRewards).length > 0) {

            } else {

            }
        } else {
            this.notificationService.showNotification("Profile UpTo date already", 'success')
        }*/
    }
}