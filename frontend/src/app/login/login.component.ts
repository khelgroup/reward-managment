import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {NotificationService} from '../services/notification.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: any = {};
    roleOptions = [
        {
            key: 'Player',
            value: 'Player'
        },
        {
            key: 'User',
            value: 'User'
        }
    ];
    constructor(private userService: UserService,
                private notificationService: NotificationService,
                private router: Router) {
    }

    ngOnInit() {
        // this.user.role = '';
        let accessToken = localStorage.getItem('token');
        if (!accessToken) {
            this.router.navigate(['/login']);
        } else {
            this.router.navigate(['/dashboard']);
        }
    }

    userLogin() {
        this.userService.login(this.user)
            .subscribe((response) => {
                console.log('this.user',response.data)
                if (!response.error) {
                    this.user = {};
                    localStorage.setItem('token', response.data.authToken);
                    if(response.data.userDetails.userId) {
                        localStorage.setItem('userId', response.data.userDetails.userId);
                    }else{
                        localStorage.setItem('userId', response.data.userDetails.playerId);
                    }
                    localStorage.setItem('role', response.data.userDetails.role);
                    this.router.navigate(['/dashboard']);
                    this.notificationService.showNotification(response.message, 'success');
                } else {
                    this.notificationService.showNotification(response.message, 'danger');
                }
            });
    }
}
