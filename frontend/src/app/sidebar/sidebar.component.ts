import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}


export const ROUTES: RouteInfo[] = [];


// {path: '/rewardmaster', title: 'Reward Master', icon: 'pe-7s-rocket', class: ''},
// {path: '/transaction', title: 'Transaction', icon: 'pe-7s-rocket', class: ''},

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    userRole;

    constructor() {
    }

    ngOnInit() {
        if (localStorage.getItem('role') === 'Player') {
            while(ROUTES.length > 0) {
                ROUTES.pop();
            }
            ROUTES.push({path: '/playerdetails', title: 'Player Info', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/playerhistory', title: 'Reward History', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/allrewards', title: 'All Rewards', icon: 'pe-7s-rocket', class: ''})
        } else {
            while(ROUTES.length > 0) {
                ROUTES.pop();
            }
            ROUTES.push({path: '/rewardmaster', title: 'Reward Master', icon: 'pe-7s-rocket', class: ''})
        }
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
