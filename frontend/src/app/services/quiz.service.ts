import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class QuizService {

    api = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getCategory(pageNumber, pageSize) {
        return this.http.get<any>(this.api + '/category?pg=' + pageNumber + '&pgsize=' + pageSize);
    }

    addCategory(data) {
        return this.http.post<any>(this.api + '/category/create', data);
    }

    editCategory(data) {
        return this.http.post<any>(this.api + '/category/update', data);
    }

    deleteCategory(data) {
        return this.http.post<any>(this.api + '/category/delete', data);
    }

    getLevel(pageNumber, pageSize) {
        return this.http.get<any>(this.api + '/level?pg=' + pageNumber + '&pgsize=' + pageSize);
    }

    addLevel(data) {
        return this.http.post<any>(this.api + '/level/create', data);
    }

    editLevel(data) {
        return this.http.post<any>(this.api + '/level/update', data);
    }

    deleteLevel(data) {
        return this.http.post<any>(this.api + '/level/delete', data);
    }

    addQuestion(data) {
        return this.http.post<any>(this.api + '/questionaries/create', data);
    }

    getQuestion(pageNumber, pageSize) {
        return this.http.get<any>(this.api + '/questionaries?pg=' + pageNumber + '&pgsize=' + pageSize);
    }

    editQuestion(data) {
        return this.http.post<any>(this.api + '/questionaries/update', data);
    }


    deleteQuestion(data) {
        return this.http.post<any>(this.api + '/questionaries/delete', data);
    }

    questinGetById(id) {
        return this.http.get<any>(this.api + '/questionaries/' + id);
    }

    addWheel(data) {
        return this.http.post<any>(this.api + '/wheel/create', data);
    }

    getWheel(pageNumber, pageSize, wheelType) {
        return this.http.get<any>(this.api + '/wheel?pg=' + pageNumber + '&pgsize=' + pageSize + '&wheelType=' + wheelType);
    }

    editWheel(data) {
        return this.http.post<any>(this.api + '/wheel/update', data);
    }

    deleteWheel(data) {
        return this.http.post<any>(this.api + '/wheel/delete', data);
    }

    addWheelType(data) {
        return this.http.post<any>(this.api + '/wheeltype/create', data);
    }

    getWheelType() {
        return this.http.get<any>(this.api + '/wheeltype');
    }

    editWheelType(data) {
        return this.http.post<any>(this.api + '/wheeltype/update', data);
    }

    deleteWheelType(data) {
        return this.http.post<any>(this.api + '/wheeltype/delete', data);
    }

}
