import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {RouterModule} from '@angular/router';

import {AppRoutingModule} from './app.routing';
import {NavbarModule} from './shared/navbar/navbar.module';
import {FooterModule} from './shared/footer/footer.module';
import {SidebarModule} from './sidebar/sidebar.module';

import {LoginModule} from './login/login.module';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {UserService} from './services/user.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {JwtInterceptor} from "./_helpers/jwt.interceptor";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        HttpModule,
        NavbarModule,
        FooterModule,
        SidebarModule,
        AppRoutingModule,
        HttpClientModule,
        LoginModule,
        ToastrModule.forRoot(),
        NgxDatatableModule
    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
    ],
    providers: [ UserService, JwtInterceptor ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
