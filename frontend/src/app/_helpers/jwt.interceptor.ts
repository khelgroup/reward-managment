import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private router: Router) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const jwt = localStorage.getItem('token');
        alert('heeeeeeeeeee')
        if (!!jwt) {
            req = req.clone({
                setHeaders: {
                    authToken: `${jwt}`
                }
            });
        }
        return next.handle(req);
    }

    /*intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const token = localStorage.getItem('token');
        console.log('token0', token)
        if (token) {
            request = request.clone({
                setHeaders: {
                    authToken: `${token}`
                }
            });
        } else {
            this.router.navigate(['/login']);
        }
        return next.handle(request);
    }*/
}
