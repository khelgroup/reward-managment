import {Routes} from '@angular/router';

import {HomeComponent} from '../../home/home.component';
import {ConfigurationComponent} from '../../configuration/configuration.component';
import {TransactionComponent} from '../../transaction/transaction.component';
import {PlayerrewardlistComponent} from "../../playerrewardlist/playerrewardlist.component";
import {AllRewardsComponent} from "../../all-rewards/all-rewards.component";


export const AdminLayoutRoutes: Routes = [
    {path: 'dashboard', component: HomeComponent},
    {path: 'rewardmaster', component: ConfigurationComponent},
    {path: 'playerdetails', component: TransactionComponent},
    {path: 'playerhistory', component: PlayerrewardlistComponent},
    {path: 'allrewards', component: AllRewardsComponent},
];
