import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LbdModule} from '../../lbd/lbd.module';
import {NguiMapModule} from '@ngui/map';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {HomeComponent} from '../../home/home.component';
import {UserComponent} from '../../user/user.component';
import {TablesComponent} from '../../tables/tables.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {MapsComponent} from '../../maps/maps.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';
import {QuizModule} from '../../quiz/quiz.module';
import {ConfigurationComponent} from '../../configuration/configuration.component';
import {TransactionComponent} from '../../transaction/transaction.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PlayerrewardlistComponent} from "../../playerrewardlist/playerrewardlist.component";
import {AllRewardsComponent} from "../../all-rewards/all-rewards.component";


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        LbdModule,
        QuizModule,
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
        NgxDatatableModule
    ],
    declarations: [
        HomeComponent,
        UserComponent,
        TablesComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        UpgradeComponent,
        ConfigurationComponent,
        TransactionComponent,
        PlayerrewardlistComponent,
        AllRewardsComponent,
    ]
})

export class AdminLayoutModule {
}
