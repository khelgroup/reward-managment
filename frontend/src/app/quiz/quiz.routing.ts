import { Routes } from '@angular/router';
import {CategoryComponent} from './category/category.component';

export const QuestionaryRoutes: Routes = [
    { path: 'category',  component: CategoryComponent },
];
