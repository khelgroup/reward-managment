import {Component, OnInit} from '@angular/core';
import {QuizService} from '../../services/quiz.service';
import {NotificationService} from '../../services/notification.service';
import {cursorTo} from 'readline';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    rows: any = [];
    totalSize;
    currentPage = 0;
    pageSize = 5;
    display = 'none';
    categoryName;
    btndisable = true;
    editBlock = false;
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '10',
            value: 10
        },
        {
            key: '20',
            value: 20
        }
    ];
    editData: any = {};

    constructor(private quizService: QuizService,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.getCategory(this.currentPage, this.pageSize);
    }

    getCategory(pageNumber, pageSize) {
        this.quizService.getCategory(pageNumber, pageSize)
            .subscribe((response) => {
                    console.log('response', response)
                    if (!response.error) {
                        this.rows = response.data.results;
                        this.totalSize = response.data.totalRecords;
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }
                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    addCategory() {
        this.editBlock = false;
        this.display = 'block';
    }

    onCloseHandled() {
        this.display = 'none';
        this.categoryName = null;
    }

    checkValue() {
        if (this.categoryName === null || this.categoryName === undefined || this.categoryName === '') {
            this.btndisable = true;
        } else {
            this.btndisable = false;
        }
    }

    saveCategory() {
        this.display = 'none';
        if (this.categoryName && this.editBlock === false) {
            let data = {
                categoryType: this.categoryName
            };
            this.quizService.addCategory(data)
                .subscribe((response) => {
                        if (!response.error) {
                            this.notificationService.showNotification(response.message, 'success');
                            this.getCategory(this.currentPage, this.pageSize);
                            this.categoryName = null;
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                        }
                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        } else if (this.categoryName && this.editBlock === true) {
            let data = {
                categoryId: this.editData.categoryId,
                categoryType: this.categoryName
            };
            this.quizService.editCategory(data)
                .subscribe((response) => {
                        if (!response.error) {
                            this.notificationService.showNotification(response.message, 'success');
                            this.getCategory(this.currentPage, this.pageSize);
                            this.categoryName = null;
                        } else {
                            this.notificationService.showNotification(response.message, 'danger')
                        }

                    },
                    error1 => {
                        console.log(error1);
                        this.notificationService.showNotification(error1, 'danger');
                    })
        }
    }

    editCategory(data) {
        console.log('edit', data);
        this.display = 'block';
        this.editData = data;
        this.categoryName = data.categoryType;
        this.editBlock = true;
        this.btndisable = false;
    }

    deleteCategory(data) {
        console.log('delete', data.categoryId);
        let value = {
            categoryId: data.categoryId
        };
        this.quizService.deleteCategory(value)
            .subscribe((response) => {
                    if (!response.error) {
                        this.notificationService.showNotification(response.message, 'success');
                        this.getCategory(this.currentPage, this.pageSize);
                    } else {
                        this.notificationService.showNotification(response.message, 'danger')
                    }

                },
                error1 => {
                    console.log(error1);
                    this.notificationService.showNotification(error1, 'danger');
                })
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getCategory(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getCategory(e.offset, e.pageSize);
    }
}
