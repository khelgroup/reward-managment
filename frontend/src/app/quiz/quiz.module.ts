import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuestionaryRoutes} from './quiz.routing';
import {RouterModule} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CategoryComponent} from './category/category.component';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(QuestionaryRoutes),
        FormsModule,
        NgxDatatableModule,
        NgbModule.forRoot(),
        // BrowserAnimationsModule, // required animations module
        // ToastrModule added
    ],
    declarations: [
        CategoryComponent,
    ]
})
export class QuizModule {
}
