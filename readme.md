**How to run Project**

- start front end by `ng serve`
- and backend with `node app.js`

** **  
**create User First**

     API: http://localhost:3000/api/v1/users/createUser
     
- Hit this api with body:
     `{
        "firstName":"xyz",
        "password":"abc!1234",
        "role":"player"
     }`
- `role` is important. its value must be `player` or `admin`, bcz based on that your dashboard menu decided
** **  

**For view admin side**
    
  - login into frontend with your admin credential.
  - add your rewards. **edit/delete** options are not working from list right now
  - this is done from admin side. 
** **  

**For view player side**
    
  - login into frontend with your player credential.
  - right now we only provide profile update rewards for like (lastName, email, mobileNumber).
  - if your profile page this field are empty and you update it then you receive your rewards
  - you can see your reward history in second tab
  - and in third tab you can see all type of rewards with your claim rewards
