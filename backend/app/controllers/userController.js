const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
const {isNullOrUndefined} = require('util');
const tokenLib = require('../libs/tokenLib')

const moment = require('moment')
var cron = require('node-cron');

var nodeMailer = require('nodemailer');

var transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'groupkhel@gmail.com',
        pass: 'Khelgroup@1'
    }
});

let mailOptions = {
    from: 'groupkhel@gmail.com',
    to: 'abhikabrawala@gmail.com',
    subject: 'subject',
    //text: 'New Register Entry',
    html: 'html', // html body
}


/* Models */
const UserModel = mongoose.model('User');
const tokenCol = mongoose.model('tokenCollection');
const Player = mongoose.model('Player');

// create User function
let createUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.password && req.body.firstName) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "password or firstName missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createUsers = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            body['userId'] = shortid.generate();
            body['firstName'] = req.body.firstName;
            body['lastName'] = req.body.lastName ? req.body.lastName : '';
            body['password'] = req.body.password;
            body['email'] = req.body.email ? req.body.email : '';
            body['mobileNumber'] = req.body.mobileNumber ? req.body.mobileNumber : '';
            body['createdOn'] = new Date();
            UserModel.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create user", "userController => createUsers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUser function

    validatingInputs()
        .then(createUsers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "User Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}

// login User function
let loginUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs", req.body);
        return new Promise((resolve, reject) => {
            if (req.body.firstName && req.body.password && req.body.role) {
                resolve();
            } else {
                let apiResponse = response.generate(true, "password or firstName or role missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findUsers = () => {
        console.log("findUsers");
        return new Promise((resolve, reject) => {
            let model;
            if(req.body.role === 'User'){
                model = UserModel
            }else{
                model = Player
            }
            model.find({firstName: req.body.firstName}, function (err, userDetails) {
                if (err) {
                    logger.error("Failed to find users", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "Failed to find user", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(userDetails)) {
                    logger.error("User not found", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "User not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(userDetails[0]);
                    console.log("userDetails",userDetails);
                }
            });
        });
    } // end of findUsers function

    let pwdMatch = (userDetails) => {
        console.log("pwdMatch");
        return new Promise((resolve, reject) => {
            let password = req.body.password
            userDetails.comparePassword(password, function (err, match) {
                if (err) {
                    logger.error("Wrong Passwordd", "userController => pwdMatch()", 5);
                    let apiResponse = response.generate(true, "Wrong Password", 500, null);
                    reject(apiResponse);
                } else {
                    if (match === true) {
                        resolve(userDetails);
                    } else {
                        logger.error("Wrong Password", "userController => pwdMatch()", 5);
                        let apiResponse = response.generate(true, "Wrong Password", 500, null);
                        reject(apiResponse);
                    }
                }
            });
        });
    } // end of pwdMatch function

    let generateToken = (user) => {
        console.log("generateToken", user);
        return new Promise((resolve, reject) => {
            tokenLib.generateToken(user, (err, tokenDetails) => {
                if (err) {
                    logger.error("Failed to generate token", "userController => generateToken()", 10);
                    let apiResponse = response.generate(true, "Failed to generate token", 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = user.toObject()
                    delete finalObject._id;
                    delete finalObject.__v;
                    if(user.playerId){
                        tokenDetails.playerId = user.playerId;
                    }else{
                        tokenDetails.userId = user.userId
                    }
                    tokenDetails.userDetails = finalObject;
                    resolve(tokenDetails);
                }
            });
        });
    }; // end of generateToken

    let saveToken = (tokenDetails) => {
        console.log("saveToken", tokenDetails);
        return new Promise((resolve, reject) => {
            tokenCol.findOne({$or:[{userId: tokenDetails.userId},{playerId: tokenDetails.playerId}]})
                .exec((err, retrieveTokenDetails) => {
                    if (err) {
                        let apiResponse = response.generate(true, "Failed to save token", 500, null);
                        reject(apiResponse);
                    }
                    // player is logging for the first time
                    else if (check.isEmpty(retrieveTokenDetails)) {
                        let id;
                        let name;
                        if(tokenDetails.playerId){
                            id = tokenDetails.playerId;
                        }else{
                            id = tokenDetails.userId;
                        }
                        let newAuthToken = new tokenCol({
                            userId: id,
                            authToken: tokenDetails.token,
                            // we are storing this is due to we might change this from 15 days
                            tokenSecret: tokenDetails.tokenSecret,
                            tokenGenerationTime: time.now()
                        });

                        newAuthToken.save((err, newTokenDetails) => {
                            if (err) {
                                let apiResponse = response.generate(true, "Failed to save token", 500, null);
                                reject(apiResponse);
                            } else {
                                let responseBody = {
                                    authToken: newTokenDetails.authToken,
                                    userDetails: tokenDetails.userDetails
                                };
                                resolve(responseBody);
                            }
                        });
                    }
                    // user has already logged in need to update the token
                    else {
                        retrieveTokenDetails.authToken = tokenDetails.token;
                        retrieveTokenDetails.tokenSecret = tokenDetails.tokenSecret;
                        retrieveTokenDetails.tokenGenerationTime = time.now();
                        retrieveTokenDetails.save((err, newTokenDetails) => {
                            if (err) {
                                let apiResponse = response.generate(true, "Failed to save token", 500, null);
                                reject(apiResponse);
                            } else {
                                delete tokenDetails._id;
                                delete tokenDetails.__v;
                                let responseBody = {
                                    authToken: newTokenDetails.authToken,
                                    userDetails: tokenDetails.userDetails
                                };
                                resolve(responseBody);
                            }
                        });
                    }
                });
        });

    }; // end of saveToken

    validatingInputs()
        .then(findUsers)
        .then(pwdMatch)
        .then(generateToken)
        .then(saveToken)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Login Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of login user function


// logout user function
let logoutUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.userId || req.body.playerId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "userId missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findToken = () => {
        console.log("findToken");
        return new Promise((resolve, reject) => {
            tokenCol.findOne({$or:[{userId: req.body.userId},{playerId: req.body.playerId}]}, function (err, tokenDetails) {
                if (err) {
                    logger.error("Failed to find token", "userController => findToken()", 5);
                    let apiResponse = response.generate(true, "Failed to find token", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(tokenDetails)) {
                    logger.error("token not found", "userController => findToken()", 5);
                    let apiResponse = response.generate(true, "token not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(tokenDetails);
                }
            });
        });
    } // end of findPlayers function

    let findRemove = (tokenDetails) => {
        console.log("findRemove");
        return new Promise((resolve, reject) => {
            tokenCol.findOneAndRemove({$or:[{userId: tokenDetails.userId},{playerId: tokenDetails.playerId}]})
                .exec((err, nwetokenDetail) => {
                    if (err) {
                        logger.error("Error while remove token", "userController => findRemove()", 5);
                        let apiResponse = response.generate(true, "Error while remove token", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(nwetokenDetail)) {
                        logger.error("No Token Remove", "userController => findRemove()", 5);
                        let apiResponse = response.generate(true, "No Token Remove", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(nwetokenDetail);
                    }
                });
        });
    } // end of pwdMatch function

    validatingInputs()
        .then(findToken)
        .then(findRemove)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Logout Successfully!!", 200, null);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of the logout function

module.exports = {
    loginUser: loginUser,
    createUser: createUser,
    logoutUser: logoutUser,

}// end exports
