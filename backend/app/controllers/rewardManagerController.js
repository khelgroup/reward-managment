const mongoose = require('mongoose'), Schema = mongoose.Schema;
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');
let data = ['Profile pic', 'Bio-data', 'Birthdate', 'State', 'Profile name','Deposit','Withdraw','Kyc']
var faker = require('faker');

/* Models */
const User = mongoose.model('User');
const RewardsMaster = mongoose.model('rewardMaster');
const PlayerReward = mongoose.model('playersRewardHistory');
const tokenCol = mongoose.model('tokenCollection');

// var mongoose = require('mongoose');
/*var collections = mongoose.connections[0].collections;
var names = [];

Object.keys(collections).forEach(function(k) {
    names.push(k);
});

console.log(names);
let abc= [];
mongoose.modelNames().forEach(function (modelname) {
    console.log('modelname', modelname);
    console.log('abc', mongoose.model(modelname).schema.obj);
    abc.push(mongoose.model(modelname).schema.obj);
})*/
// console.log('abc', abc);

// createRewards function
let createRewardMaster = () => {
    console.log("createRewardMaster");
    return new Promise((resolve, reject) => {
        let body = [];
        data.filter((x) => {
            let json = {}
            json['rewardMasterId'] = shortid.generate();
            json['rewardName'] = x;
            json['createdOn'] = new Date();
            body.push(json)
        })
        RewardsMaster.create(body, function (err, response) {
            if (err) {
                logger.error("Failed to create players", "playerController => findPlayers()", 5);
                let apiResponse = response.generate(true, err, 500, null);
                reject(apiResponse);
            } else {
                resolve(response);
            }
        });
    });
}
// end of createRewards function

//getRewards with condition
let getRewards = (req, res) => {

    let findAllRewards = () => {
        console.log("findAllRewards");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({}, {}, {})
                .select('-__v -_id')
                .exec((err, RewardDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve Reward", "rewardManagerController => findAllRewards()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve Reward", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(RewardDetails)) {
                        logger.error("No rewards found", "rewardManagerController => findAllRewards()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        console.log('RewardDetails', RewardDetails)
                        createRewardMaster()
                            .then(() => resolve())
                            .catch((e) => {
                                reject(e);
                            })
                    } else {
                        resolve(RewardDetails);
                    }
                });
        })
    }// end of findTransaction Functions

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit", req.query.pg, req.query.pgSize);
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    // find players by configuration
    // let findUser = (data) => {
    //     console.log("findUser");
    //     return new Promise((resolve, reject) => {
    //         User.find({userId: req.query.userId}, {}, {})
    //             .select('-__v -_id')
    //             .exec((err, playerDetails) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
    //                     reject(apiResponse);
    //                 } else if (check.isEmpty(playerDetails)) {
    //                     logger.error("No User found", "userController => findPlayers()", 5);
    //                     let apiResponse = response.generate(true, "No User found with this userId", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     resolve(data);
    //                 }
    //             })
    //     });
    // }

    // find Transaction
    let findRewards = (data) => {
        console.log("findRewards");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, transactionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve transactions", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(transactionDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(transactionDetails);
                    }
                });

        });
    } // end of findTransaction Functions

    // find findRewardsCount
    let findRewardsCount = (data) => {
        console.log("findRewardsCount");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({}, {}, {})
                .count()
                .exec((err, transactionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve transactions", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(transactionDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {};
                        final.results = data;
                        final.count = transactionDetails;
                        resolve(final);
                    }
                });

        });
    } // end of findTransaction Functions

    findAllRewards()
        .then(getSizeLimit)
        .then(findRewards)
        .then(findRewardsCount)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Rewards List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getRewards

//listtables
let listtables = (req, res) => {

    // find findtableslist
    let list = () => {
        console.log("findtableslist");
        return new Promise((resolve, reject) => {
            var names = [];
            mongoose.modelNames().forEach(function (modelname) {
                names.push(modelname);
            })
            resolve(names);
        });
    } // end of findtableslist Functions

    list()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Rewards List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of listtables

//listfields
let listfields = (req, res) => {
    // find findfieldlist
    let list = () => {
        console.log("findfieldlist");
        return new Promise((resolve, reject) => {
            var names = [];
            var collections = mongoose.connections[0].collections;
            mongoose.modelNames().forEach(function (modelname) {
                if (modelname === req.query.rewardName) {
                    resolve(mongoose.model(modelname).schema.obj)
                }
            })
        });
    } // end of findfieldlist Functions

    list()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Rewards List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of listfields

//getAllRewards
let getAllRewards = (req, res) => {

    // find Transaction
    let findRewards = (data) => {
        console.log("findRewards");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({}, {}, {})
                .select('-__v -_id')
                .exec((err, transactionDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve transactions", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve transaction", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(transactionDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {};
                        final.results = transactionDetails;
                        resolve(final);
                    }
                });

        });
    } // end of findTransaction Functions

    findRewards()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get All Rewards List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getAllRewards

//updateRewards
let updateRewards = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.rewardMasterId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "rewardMasterId missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let updateReward = () => {
        console.log("updateProfile");
        return new Promise((resolve, reject) => {
            let body = {};
            body['rewardType'] = req.body.rewardType;
            body['rewardAmount'] = req.body.rewardAmount;
            body['isActive'] = req.body.isActive;
            RewardsMaster.findOneAndUpdate({rewardMasterId: req.body.rewardMasterId}, body, {new: true})
                .exec(function (err, res) {
                    if (err) {
                        logger.error("Failed to update Player history", "userController => updateProfile()", 5);
                        let apiResponse = response.generate(true, "Failed to update Player history", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve();
                    }
                })
        })
    }

    validatingInputs()
        .then(updateReward)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Update Rewards Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of updateRewards


module.exports = {

    createRewardMaster: createRewardMaster,
    getRewards: getRewards,
    listtables: listtables,
    listfields: listfields,
    getAllRewards: getAllRewards,
    updateRewards: updateRewards
}// end exports
