const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib');
const logger = require('./../libs/loggerLib');
const check = require('../libs/checkLib');
const tokenLib = require('../libs/tokenLib');
var faker = require('faker');

/* Models */

const User = mongoose.model('User');

const RewardsMaster = mongoose.model('rewardMaster');
const PlayerReward = mongoose.model('playersRewardHistory');
const wallet = mongoose.model('Wallet');
const Player = mongoose.model('Player');
const tokenCol = mongoose.model('tokenCollection');
const Withdraw = mongoose.model('Withdraw');
const Deposit = mongoose.model('Deposit');
const Kyc = mongoose.model('Kyc');

// create User function
let createUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.password && req.body.firstName) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "password or firstName missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createUsers = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            body['playerId'] = shortid.generate();
            body['firstName'] = req.body.firstName;
            body['password'] = req.body.password;
            body['state'] = req.body.state;
            body['otp'] = req.body.otp;
            body['coverPhoto'] = req.body.coverPhoto;
            body['email'] = req.body.email ? req.body.email : '';
            body['profileName'] = req.body.profileName ? req.body.profileName : '';
            body['profilePhoto'] = req.body.profilePhoto ? req.body.profilePhoto : '';
            body['bio'] = req.body.bio ? req.body.bio : '';
            body['deviceId'] = req.body.deviceId ? req.body.deviceId : '';
            body['status'] = req.body.status ? req.body.status : '';
            body['socialPicture'] = req.body.socialPicture ? req.body.socialPicture : '';
            body['dob'] = req.body.dob ? req.body.dob : '';
            body['socialId'] = req.body.socialId ? req.body.socialId : '';
            body['socialName'] = req.body.socialName ? req.body.socialName : '';
            body['mobileNumber'] = req.body.mobileNumber ? req.body.mobileNumber : '';
            body['socialAccount'] = req.body.socialAccount ? req.body.socialAccount : '';
            body['createdOn'] = new Date();
            Player.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create player", "playerController => createPlayer()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUser function

    validatingInputs()
        .then(createUsers)
        .then((resolve) => {
            // handler('s');
            let apiResponse = response.generate(false, "Player Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}

// // login User function
// let loginUser = (req, res) => {
//
//     let validatingInputs = () => {
//         console.log("validatingInputs");
//         return new Promise((resolve, reject) => {
//             if (req.body.firstName && req.body.password && (req.body.role === 'Player')) {
//                 resolve(req);
//             } else {
//                 let apiResponse = response.generate(true, "password or firstName or role missing", 400, null);
//                 reject(apiResponse);
//             }
//         });
//     }; // end of validatingInputs
//
//     let findUsers = () => {
//         console.log("findUsers");
//         return new Promise((resolve, reject) => {
//             Player.find({firstName: req.body.firstName}, function (err, userDetails) {
//                 if (err) {
//                     logger.error("Failed to find users", "userController => findUsers()", 5);
//                     let apiResponse = response.generate(true, "Failed to find user", 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(userDetails)) {
//                     logger.error("User not found", "userController => findUsers()", 5);
//                     let apiResponse = response.generate(true, "User not found", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     resolve(userDetails[0]);
//                 }
//             });
//         });
//     } // end of findUsers function
//
//     let pwdMatch = (userDetails) => {
//         console.log("pwdMatch");
//         return new Promise((resolve, reject) => {
//             let password = req.body.password
//             userDetails.comparePassword(password, function (err, match) {
//                 if (err) {
//                     logger.error("Wrong Passwordd", "userController => pwdMatch()", 5);
//                     let apiResponse = response.generate(true, "Wrong Password", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     if (match === true) {
//                         resolve(userDetails);
//                     } else {
//                         logger.error("Wrong Password", "userController => pwdMatch()", 5);
//                         let apiResponse = response.generate(true, "Wrong Password", 500, null);
//                         reject(apiResponse);
//                     }
//                 }
//             });
//         });
//     } // end of pwdMatch function
//
//     let generateToken = (user) => {
//         console.log("generateToken", user);
//         return new Promise((resolve, reject) => {
//             tokenLib.generateToken(user, (err, tokenDetails) => {
//                 if (err) {
//                     logger.error("Failed to generate token", "userController => generateToken()", 10);
//                     let apiResponse = response.generate(true, "Failed to generate token", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     let finalObject = user.toObject()
//                     delete finalObject._id;
//                     delete finalObject.__v;
//                     tokenDetails.userId = user.userId;
//                     tokenDetails.userDetails = finalObject;
//                     resolve(tokenDetails);
//                 }
//             });
//         });
//     }; // end of generateToken
//
//     let saveToken = (tokenDetails) => {
//         console.log("saveToken");
//         return new Promise((resolve, reject) => {
//             tokenCol.findOne({userId: tokenDetails.userId})
//                 .exec((err, retrieveTokenDetails) => {
//                     if (err) {
//                         let apiResponse = response.generate(true, "Failed to save token", 500, null);
//                         reject(apiResponse);
//                     }
//                     // player is logging for the first time
//                     else if (check.isEmpty(retrieveTokenDetails)) {
//                         let newAuthToken = new tokenCol({
//                             userId: tokenDetails.userId,
//                             authToken: tokenDetails.token,
//                             // we are storing this is due to we might change this from 15 days
//                             tokenSecret: tokenDetails.tokenSecret,
//                             tokenGenerationTime: time.now()
//                         });
//
//                         newAuthToken.save((err, newTokenDetails) => {
//                             if (err) {
//                                 let apiResponse = response.generate(true, "Failed to save token", 500, null);
//                                 reject(apiResponse);
//                             } else {
//                                 let responseBody = {
//                                     authToken: newTokenDetails.authToken,
//                                     userDetails: tokenDetails.userDetails
//                                 };
//                                 resolve(responseBody);
//                             }
//                         });
//                     }
//                     // user has already logged in need to update the token
//                     else {
//                         retrieveTokenDetails.authToken = tokenDetails.token;
//                         retrieveTokenDetails.tokenSecret = tokenDetails.tokenSecret;
//                         retrieveTokenDetails.tokenGenerationTime = time.now();
//                         retrieveTokenDetails.save((err, newTokenDetails) => {
//                             if (err) {
//                                 let apiResponse = response.generate(true, "Failed to save token", 500, null);
//                                 reject(apiResponse);
//                             } else {
//                                 delete tokenDetails._id;
//                                 delete tokenDetails.__v;
//                                 let responseBody = {
//                                     authToken: newTokenDetails.authToken,
//                                     userDetails: tokenDetails.userDetails
//                                 };
//                                 resolve(responseBody);
//                             }
//                         });
//                     }
//                 });
//         });
//
//     }; // end of saveToken
//
//     validatingInputs()
//         .then(findUsers)
//         .then(pwdMatch)
//         .then(generateToken)
//         .then(saveToken)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Login Successfully!!", 200, resolve);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of login user function

// logout user function
// let logoutUser = (req, res) => {
//
//     let validatingInputs = () => {
//         console.log("validatingInputs");
//         return new Promise((resolve, reject) => {
//             if (req.body.userId) {
//                 resolve(req);
//             } else {
//                 let apiResponse = response.generate(true, "userId missing", 400, null);
//                 reject(apiResponse);
//             }
//         });
//     }; // end of validatingInputs
//
//     let findToken = () => {
//         console.log("findToken");
//         return new Promise((resolve, reject) => {
//             tokenCol.findOne({userId: req.body.userId}, function (err, tokenDetails) {
//                 if (err) {
//                     logger.error("Failed to find token", "userController => findToken()", 5);
//                     let apiResponse = response.generate(true, "Failed to find token", 500, null);
//                     reject(apiResponse);
//                 } else if (check.isEmpty(tokenDetails)) {
//                     logger.error("token not found", "userController => findToken()", 5);
//                     let apiResponse = response.generate(true, "token not found", 500, null);
//                     reject(apiResponse);
//                 } else {
//                     resolve(tokenDetails);
//                 }
//             });
//         });
//     } // end of findPlayers function
//
//     let findRemove = (tokenDetails) => {
//         console.log("findRemove");
//         return new Promise((resolve, reject) => {
//             tokenCol.findOneAndRemove({userId: tokenDetails.userId})
//                 .exec((err, nwetokenDetail) => {
//                     if (err) {
//                         logger.error("Error while remove token", "userController => findRemove()", 5);
//                         let apiResponse = response.generate(true, "Error while remove token", 500, null);
//                         reject(apiResponse);
//                     } else if (check.isEmpty(nwetokenDetail)) {
//                         logger.error("No Token Remove", "userController => findRemove()", 5);
//                         let apiResponse = response.generate(true, "No Token Remove", 500, null);
//                         reject(apiResponse);
//                     } else {
//                         resolve(nwetokenDetail);
//                     }
//                 });
//         });
//     } // end of pwdMatch function
//
//     validatingInputs()
//         .then(findToken)
//         .then(findRemove)
//         .then((resolve) => {
//             let apiResponse = response.generate(false, "Logout Successfully!!", 200, null);
//             res.send(apiResponse);
//         })
//         .catch((err) => {
//             console.log(err);
//             res.send(err);
//             res.status(err.status);
//         });
// } // end of the logout function

// getplayerinfo function
let getplayerinfo = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.query.userId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "userId is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findPlayer = () => {
        console.log("findPlayer");
        return new Promise((resolve, reject) => {
            Player.findOne({playerId: req.query.userId}, function (err, output) {
                if (err) {
                    logger.error("Failed to create players", "playerController => findPlayers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(output)) {
                    logger.error("Player not found", "playerController => findPlayers()", 5);
                    let apiResponse = response.generate(true, "Player not found", 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = output.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createPlayers function

    validatingInputs()
        .then(findPlayer)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player find Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getplayerinfo function

// updatePlayer function
let updatePlayer = (req, res) => {

    // fetchReward = () => {
    //     console.log("fetchReward");
    //     return new Promise((resolve, reject) => {
    //         RewardsMaster.find()
    //             .select('-__v -_id')
    //             .exec((err, rewards) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve rewards", "userController => fetchReward()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve rewards", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     let final = {
    //                         allRewards: rewards
    //                     };
    //                     resolve(final);
    //                 }
    //             })
    //     })
    // }
    //
    // findPlayerRewards = (data) => {
    //     console.log("findPlayerRewards");
    //     return new Promise((resolve, reject) => {
    //         PlayerReward.find({playerId: req.body.playerId})
    //             .select('-__v -_id')
    //             .exec((err, playerRewards) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve Player rewards", "userController => findPlayerRewards()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve Player rewards", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     data.playerRewards = playerRewards;
    //                     resolve(data);
    //                 }
    //             })
    //     })
    // }
    //
    // findPlayer = (data) => {
    //     console.log("findPlayer");
    //     return new Promise((resolve, reject) => {
    //         Player.find({playerId: req.body.playerId})
    //             .select('-__v -_id')
    //             .exec((err, playerInfo) => {
    //                 if (err) {
    //                     logger.error("Failed to retrieve Player", "userController => findPlayer()", 5);
    //                     let apiResponse = response.generate(true, "Failed to retrieve Player", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     data.playerInfo = playerInfo;
    //                     resolve(data);
    //                 }
    //             })
    //     })
    // }
    //
    // processData = (data) => {
    //     console.log("processData");
    //     return new Promise((resolve, reject) => {
    //         let playerInfo = data.playerInfo[0].toObject();
    //         let keys = Object.keys(playerInfo);
    //         let allPromise = [];
    //         keys.forEach((x) => {
    //             if ((playerInfo[x] === '' || playerInfo[x] === null || playerInfo[x] === 0) && req.body[x]) {
    //                 data.allRewards.filter((y, i) => {
    //                     let body = {};
    //                     body["playerRewardHistoryId"] = shortid.generate();
    //                     body["playerId"] = req.body.playerId;
    //                     body["rewardMasterId"] = y.rewardMasterId;
    //                     body["rewardType"] = y.rewardType;
    //                     body["rewardAmount"] = y.rewardAmount;
    //                     body["createdOn"] = new Date();
    //                     console.log('y.rewardName ', y["rewardName"], x)
    //                     if (((y["rewardName"] === 'Bio-data') && (x === 'bio')) || ((y["rewardName"] === 'Profile pic') && (x === 'profilePhoto')) || ((y["rewardName"] === 'Birthdate') && (x === 'dob')) || ((y["rewardName"] === 'State') && (x === 'state')) || ((y["rewardName"] === 'Profile name') && (x === 'profileName'))) {
    //                         allPromise.push(insertPlayerHistory(body));
    //                     }
    //                 })
    //             }
    //         });
    //         if(allPromise.length>0) {
    //             Promise.all(allPromise)
    //                 .then(values => {
    //                     updateProfile(req.body)
    //                         .then(() => {
    //                             resolve()
    //                         })
    //                 });
    //         } else {
    //             updateProfile(req.body)
    //                 .then(() => {
    //                     resolve()
    //                 })
    //         }
    //     })
    // }
    //
    // insertPlayerHistory = (body) => {
    //     console.log("insertPlayerHistory", body);
    //     return new Promise((resolve, reject) => {
    //         PlayerReward.create(body, function (err, response) {
    //             if (err) {
    //                 logger.error("Failed to create Player history", "userController => insertPlayerHistory()", 5);
    //                 let apiResponse = response.generate(true, "Failed to create Player history", 500, null);
    //                 reject(apiResponse);
    //             } else {
    //                 updateWallet(body)
    //                     .then(() => {
    //                         resolve();
    //                     })
    //             }
    //         })
    //     })
    // }

    updateProfile = () => {
        console.log("updateProfile");
        return new Promise((resolve, reject) => {
            Player.findOne({playerId: req.body.playerId}, {}, {new: true})
                .exec(function (err, res) {
                    if (err) {
                        logger.error("Failed to update Player history", "userController => updateProfile()", 5);
                        let apiResponse = response.generate(true, "Failed to update Player history", 500, null);
                        reject(apiResponse);
                    } else {
                        Object.assign(res, req.body).save();
                        resolve();
                    }
                })
        })
    }

    // updateWallet = (body) => {
    //     console.log("updateWallet", body);
    //     return new Promise((resolve, reject) => {
    //         let json;
    //         if (body.rewardType === 'cash') {
    //             json = {
    //                 $inc: {
    //                     bonusCashBalance: body.rewardAmount,
    //                     totalCashBalance: body.rewardAmount
    //                 }
    //             }
    //         } else if (body.rewardType === 'token') {
    //             json = {
    //                 $inc: {
    //                     totalTokenBalance: body.rewardAmount,
    //                 }
    //             }
    //         }
    //         console.log('json', json)
    //         wallet.findOneAndUpdate({playerId: body.playerId}, json, {new: true})
    //             .exec(function (err, res) {
    //                 if (err) {
    //                     logger.error("Failed to create Wallet history", "userController => insertPlayerHistory()", 5);
    //                     let apiResponse = response.generate(true, "Failed to create Wallet history", 500, null);
    //                     reject(apiResponse);
    //                 } else {
    //                     resolve();
    //                 }
    //             })
    //     })
    // }

    updateProfile()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Player Update Successfully Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of updatePlayer function

// claimrewards function
let claimrewards = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.rewardMasterId && req.body.rewardAmount) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "rewardMasterId or rewardAmount is missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let insertPlayerReward = () => {
        console.log("insertPlayerReward");
        return new Promise((resolve, reject) => {
            let body = {};
            body['rewardMasterId'] = shortid.generate();
            body['playerId'] = req.body.playerId;
            body['rewardMasterId'] = req.body.rewardMasterId;
            body['rewardAmount'] = req.body.rewardAmount;
            body['createdOn'] = new Date();
            PlayerReward.insert(body, function (err, tokenDetails) {
                if (err) {
                    logger.error("Failed to insert PlayerHistory", "playerController => insertPlayerReward()", 5);
                    let apiResponse = response.generate(true, "Failed to insert PlayerHistory", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(tokenDetails);
                }
            });
        });
    } // end of findPlayers function

    validatingInputs()
        .then(insertPlayerReward)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Insert Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of claimrewards function

// getclaimrewards function
let getclaimrewards = (req, res) => {

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    // find players by configuration
    let findUser = (data) => {
        console.log("findUser");
        return new Promise((resolve, reject) => {
            Player.find({playerId: req.query.userId}, {}, {})
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No User found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No User found with this userId", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(data);
                    }
                })
        });
    }

    let findPlayerRewards = (data) => {
        console.log("findPlayerRewards");
        return new Promise((resolve, reject) => {
            PlayerReward.find({playerId: req.query.userId}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find PlayerHistory", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPReward Historylayers function

    let findPlayerRewardscount = (data) => {
        console.log("findPlayerRewardscount");
        return new Promise((resolve, reject) => {
            let body = {playerId: req.query.userId};
            PlayerReward.find(body)
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let count = PlayerReward.find(body).count();
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of findPlayers function

    let findPlayerRewardsdetail = (data) => {
        console.log("findPlayerRewardsdetail");
        return new Promise((resolve, reject) => {
            let ids = (data.results).map(x => x.rewardMasterId);
            RewardsMaster.find({rewardMasterId: {"$in": ids}})
                .exec((err, allRewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => findPlayerRewardsdetail()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];
                        (data.results).filter((x) => {
                            x = x.toObject();
                            allRewardsDetails.filter((y) => {
                                y = y.toObject();
                                if (x.rewardMasterId === y.rewardMasterId) {
                                    let js = {
                                        rewardColumn: y["rewardColumn"],
                                        rewardType: y["rewardType"],
                                        rewardName: y["rewardName"],
                                        playerId: x["playerId"],
                                        playerRewardHistoryId: x["playerRewardHistoryId"],
                                        createdOn: x["createdOn"],
                                        rewardAmount: x["rewardAmount"],
                                        rewardMasterId: x["rewardMasterId"]
                                    };
                                    result.push(js);
                                }
                            })
                        });
                        data.results = result;
                        resolve(data);
                    }
                });
        });
    } // end of findPlayers function

    getSizeLimit()
        .then(findUser)
        .then(findPlayerRewards)
        .then(findPlayerRewardscount)
        .then(findPlayerRewardsdetail)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get rewards Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getclaimrewards function

let getallrewards = (req, res) => {

    // get size Limit
    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    // find players by configuration
    let findUser = (data) => {
        console.log("findUser");
        return new Promise((resolve, reject) => {
            Player.find({playerId: req.query.userId}, {}, {})
                .select('-__v -_id')
                .exec((err, playerDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve players", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve players", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(playerDetails)) {
                        logger.error("No User found", "userController => findPlayers()", 5);
                        let apiResponse = response.generate(true, "No User found with this userId", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(data);
                    }
                })
        });
    }

    let findAllRewards = (data) => {
        console.log("findPlayerRewards");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: -1}
            })
                .select('-__v -_id')
                .exec((err, rewardsDetails) => {
                    if (err) {
                        console.log('err', err)
                        logger.error("Failed to find PlayerHistory", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(rewardsDetails)) {
                        logger.error("No rewards found", "userController => findTransaction()", 5);
                        let apiResponse = response.generate(true, "No rewards found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(rewardsDetails);
                    }
                });
        });
    } // end of findPlayers function

    let findAllRewardscount = (data) => {
        console.log("findPlayerRewardscount");
        return new Promise((resolve, reject) => {
            RewardsMaster.find({})
                .count()
                .exec((err, rewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => insertPlayerReward()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = {
                            results: data,
                            count: rewardsDetails
                        }
                        resolve(final);
                    }
                });
        });
    } // end of findPlayers function

    let findPlayerRewardsdetail = (data) => {
        console.log("findPlayerRewardsdetail");
        return new Promise((resolve, reject) => {
            let ids = (data.results).map(x => x.rewardMasterId);
            PlayerReward.find({playerId: req.query.userId})
                .exec((err, allRewardsDetails) => {
                    if (err) {
                        logger.error("Failed to find PlayerHistory count", "playerController => findPlayerRewardsdetail()", 5);
                        let apiResponse = response.generate(true, "Failed to find PlayerHistory count", 500, null);
                        reject(apiResponse);
                    } else {
                        let result = [];

                        (data.results).filter((x) => {
                            x = x.toObject();
                            let playerRaw = allRewardsDetails.filter(y => {
                                y = y.toObject();
                                if (y.rewardMasterId === x.rewardMasterId) {
                                    return y;
                                }
                            })
                            console.log('abc', playerRaw);
                            let js = {
                                rewardColumn: x["rewardColumn"],
                                rewardType: x["rewardType"],
                                rewardName: x["rewardName"],
                                rewardAmount: x["rewardAmount"],
                                rewardMasterId: x["rewardMasterId"]
                            }
                            if (playerRaw.length > 0) {
                                js['playerId'] = playerRaw[0]["playerId"];
                                js['createdOn'] = playerRaw[0]["createdOn"];
                                // js['rewardAmount'] = playerRaw[0]["rewardAmount"];
                                js['status'] = true
                            } else {
                                js['status'] = false
                            }
                            result.push(js);
                            /*allRewardsDetails.filter((y) => {
                                y = y.toObject();
                                let check = result.filter(z => {
                                    if ((z.rewardMasterId === y.rewardMasterId)) {
                                        return z;
                                    }
                                });
                                if (check.length < 1) {
                                    let js = {
                                        rewardColumn: x["rewardColumn"],
                                        rewardType: x["rewardType"],
                                        rewardName: x["rewardName"],
                                        rewardMasterId: x["rewardMasterId"]
                                    }
                                    if (x.rewardMasterId === y.rewardMasterId) {
                                        js['playerId'] = y["playerId"];
                                        js['createdOn'] = y["createdOn"];
                                        js['rewardAmount'] = y["rewardAmount"];

                                        js['status'] = true
                                    } else {
                                        js['status'] = false
                                    }
                                    result.push(js);
                                }

                            })*/
                        });
                        data.results = result;
                        resolve(data);
                    }
                });
        });
    } // end of findPlayers function

    getSizeLimit()
        .then(findUser)
        .then(findAllRewards)
        .then(findAllRewardscount)
        .then(findPlayerRewardsdetail)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get rewards Successfully!!", 200, resolve);
            console.log('res', apiResponse);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of getallrewards function


//testing purpose
let createWithdraw = (req, res) => {
    console.log("findPlayerRewards");
    return new Promise((resolve, reject) => {
        Withdraw.create({withdrawId: shortid.generate(), playerId: req.body.playerId}, function (err, data) {
            if (err) {
                logger.error("Failed to create withdraw", "createWithdraw => insertPlayerHistory()", 5);
                let apiResponse = response.generate(true, "Failed to create withdraw", 500, null);
                res.send(apiResponse);

            } else {
                let apiResponse = response.generate(false, "withdraw added", 200, data);
                res.send(apiResponse);
            }
        })
    })
}

//testing purpose
let createDeposit = (req, res) => {
    console.log("findPlayerRewards");
    return new Promise((resolve, reject) => {
        Deposit.create({depositId: shortid.generate(), playerId: req.body.playerId}, function (err, data) {
            if (err) {
                logger.error("Failed to create deposit history", "playerController => createDeposit()", 5);
                let apiResponse = response.generate(true, "Failed to create deposit", 500, null);
                res.send(apiResponse);
            } else {
                let apiResponse = response.generate(false, "deposit added", 200, data);
                res.send(apiResponse);
            }
        })
    })
}

//testing purpose
let createKyc = (req, res) => {
    console.log("findPlayerRewards");
    return new Promise((resolve, reject) => {
        Kyc.create({kycId: shortid.generate(), playerId: req.body.playerId}, function (err, data) {
            if (err) {
                logger.error("Failed to create kyc", "playerController => createKyc()", 5);
                let apiResponse = response.generate(true, "Failed to create kyc history", 500, null);
                res.send(apiResponse);
            } else {
                let apiResponse = response.generate(false, "Kyc added", 200, data);
                res.send(apiResponse);
            }
        })
    })
}

module.exports = {
    getplayerinfo: getplayerinfo,
    updatePlayer: updatePlayer,
    claimrewards: claimrewards,
    getclaimrewards: getclaimrewards,
    getallrewards: getallrewards,
    createUser: createUser,
    createWithdraw: createWithdraw,
    createKyc: createKyc,
    createDeposit: createDeposit,
    // loginUser: loginUser,
    // logoutUser: logoutUser,
}// end exports
