const express = require('express');
const router = express.Router();
const rewardManagerController = require("./../../app/controllers/rewardManagerController");
const appConfig = require("./../../config/appConfig");
const middleware = require('../middlewares/auth');

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/rewards`;

    // defining routes.

    // Get All Rewards which are Created in RewardMaster Table in Admin Side
    app.get(`${baseUrl}/`, rewardManagerController.getRewards);

    //List of Tables from database to create new Reward.
    app.get(`${baseUrl}/listtables`, rewardManagerController.listtables);

    //List of columns of tables from database to create new Reward.
    app.get(`${baseUrl}/listfields`, rewardManagerController.listfields);

    // All the records which are created in RewardMaster table
    // app.get(`${baseUrl}/all`, rewardManagerController.getAllRewards);

    // Create Record for RewardMaster table
    app.post(`${baseUrl}/`, rewardManagerController.createRewardMaster);

    // Create Record for RewardMaster table
    app.post(`${baseUrl}/update`, rewardManagerController.updateRewards);


}
