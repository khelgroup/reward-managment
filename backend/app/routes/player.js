const express = require('express');
const router = express.Router();
const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig");
const middleware = require('../middlewares/auth');

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/players`;

    // this api use for user login with role 'player'

    // create user for system(password, firstName, role)
    app.post(`${baseUrl}/createPlayer`, playerController.createUser);


    // use to login into system as player or admin
    // app.post(`${baseUrl}/loginPlayer`, playerController.loginUser);
    //
    // // logout from system
    // app.post(`${baseUrl}/logoutPlayer`, playerController.logoutUser);

    // getOne particular player info(query params userId)
    app.get(`${baseUrl}/getplayerinfo`, playerController.getplayerinfo);

    // update player in user profile (like email,mobileNumber, lastname, etc)
    app.post(`${baseUrl}/update`, playerController.updatePlayer);

    // we didnt use it , we create it claim reward from somewhere
    // app.post(`${baseUrl}/claimrewards`, playerController.claimrewards);

    // Get Player All Rewards which are claimed.
    app.get(`${baseUrl}/getclaimrewards`, playerController.getclaimrewards);

    // Get All Rewards from RewardMaster Table which are claimed or not Claimed(Includes Status).
    app.get(`${baseUrl}/getallrewards`, playerController.getallrewards);

    // testing purpose
    app.post(`${baseUrl}/createWithdraw`, playerController.createWithdraw);

    // testing purpose
    app.post(`${baseUrl}/createDeposit`, playerController.createDeposit);

    // testing purpose
    app.post(`${baseUrl}/createKyc`, playerController.createKyc);

}
