'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let playersRewardHistorySchema = new Schema({
    playerRewardHistoryId: {
        type: String,
        default: '',
// enables us to search the record faster
        index: true,
        unique: true
    },
    // playerId
    playerId: {
        type: String,
        default: ''
    },
    // rewardMasterId of rewardMaster
    rewardMasterId: {
        type: String,
        default: ''
    },
    // rewardAmount like 10 coins/ points/ etc
    rewardAmount: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ''
    },
}, {versionKey: false});

mongoose.model('playersRewardHistory', playersRewardHistorySchema);
