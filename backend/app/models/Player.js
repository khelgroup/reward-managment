'use strict'

const shortid = require('shortid');
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    Schema = mongoose.Schema,
    SALT_WORK_FACTOR = 10;

let playerSchema = new Schema({
    playerId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    role: {
        type: String,
        default: 'Player'
    },
    firstName: {
        type: String,
        default: ''
    },
    dob: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        default: '',
        required: true
    },
    state: {
        type: String,
        default: ''
    },
    mobileNumber: {
        type: String,
        default: ''
    },
    otp: {
        type: Number,
        default: 0
    },
    coverPhoto: {
        type: String,
        default: ''
    },
    profileName: {
        type: String,
        default: ''
    },
    profilePhoto: {
        type: String,
        default: ''
    },
    bio: {
        type: String,
        default: ''
    },
    deviceId: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: ''
    },
    socialAccount: {
        type: String,
        default: ''
    },
    socialPicture: {
        type: String,
        default: ''
    },
    socialId: {
        type: String,
        default: ''
    },
    socialName: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ''
    },
}, {versionKey: false})


playerSchema.pre('save', function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

const insertPlayerHistory = (body) => {
    console.log("insertPlayerHistory", body);
        mongoose.model('playersRewardHistory').create(body)
            .then((res)=>{
                updateWallet(body)
                    .then((response) => {
                        return response;
                    })
            })
}

const updateWallet = (body) => {
    console.log("updateWallet", body);
        let json;
        if (body.rewardType === 'cash') {
            json = {
                $inc: {
                    bonusCashBalance: body.rewardAmount,
                    totalCashBalance: body.rewardAmount
                }
            }
        } else if (body.rewardType === 'token') {
            json = {
                $inc: {
                    totalTokenBalance: body.rewardAmount,
                }
            }
        }
        console.log('json', json)
        mongoose.model('Wallet').findOneAndUpdate({playerId: body.playerId}, json, {new: true})
            .then((res)=>{
                return res;
            })
}
//Pre Save for Deposit
playerSchema.pre('save', function (next) {
    const PlayerReward = mongoose.model('playersRewardHistory');
    const wallet = mongoose.model('Wallet');
    const RewardsMaster = mongoose.model('rewardMaster');
    const player = this;
    // const Player = mongoose.model('Player');
    // only hash the password if it has been modified (or is new)
    if (!player.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(player.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            player.password = hash;
            next();
        });
    });
    // do stuff
    let allPromise = [];
    if (player.isNew) {
        next()
    } else {
        console.log('no data', player.bio);
        return Promise.resolve()
            .then(() => {
                RewardsMaster.find()
                    .then((data) => {
                        let final = {
                            allRewards: data
                        };
                        return final;
                    })
                    .then((allReward) => {
                        PlayerReward.find({playerId: player.playerId})
                            .then((data) => {
                                allReward.playerRewards = data
                                return allReward;
                            })
                            .then((playerReward) => {
                                mongoose.model('Player').find({playerId: player.playerId}, {}, {})
                                    .then((data) => {
                                        playerReward.playerInfo = data;
                                        console.log('playerReward', playerReward);
                                        return playerReward;
                                    })
                                    .then((data) => {
                                        let playerInfo = data.playerInfo[0].toObject();
                                        let keys = Object.keys(playerInfo);
                                        let allPromise = [];
                                        let newPlayer = player
                                        keys.forEach((x) => {
                                            console.log('consition', x, (playerInfo[x] === '' || playerInfo[x] === null || playerInfo[x] === 0) && newPlayer[x])
                                            if ((playerInfo[x] === '' || playerInfo[x] === null || playerInfo[x] === 0) && newPlayer[x]) {
                                                data.allRewards.filter((y, i) => {
                                                    let body = {};
                                                    body["playerRewardHistoryId"] = shortid.generate();
                                                    body["playerId"] = player.playerId;
                                                    body["rewardMasterId"] = y.rewardMasterId;
                                                    body["rewardType"] = y.rewardType;
                                                    body["rewardAmount"] = y.rewardAmount;
                                                    body["createdOn"] = new Date();
                                                    console.log('y.rewardName ', y["rewardName"], x)
                                                    if (((y["rewardName"] === 'Bio-data') && (x === 'bio')) || ((y["rewardName"] === 'Profile pic') && (x === 'profilePhoto')) || ((y["rewardName"] === 'Birthdate') && (x === 'dob')) || ((y["rewardName"] === 'State') && (x === 'state')) || ((y["rewardName"] === 'Profile name') && (x === 'profileName'))) {
                                                        allPromise.push(insertPlayerHistory(body));
                                                    }
                                                })
                                            }
                                        })
                                    })
                                    .then((data) => {
                                        if(allPromise.length>0) {
                                            Promise.all(allPromise)
                                                .then(values => {
                                                   next()
                                                });
                                        } else {
                                            next()
                                        }
                                    })
                                    .catch(err => {
                                        console.log('err', err);
                                        next()
                                    })
                            })
                            .catch(err => {
                                console.log('err', err);
                                next()
                            })
                    })
                    .catch(err => {
                        console.log('err', err);
                        next()
                    })
            })
            .catch(err => {
                console.log('err', err);
                next()
            })

    }
})

playerSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) cb(err, null);
        cb(null, isMatch);
    });
};

mongoose.model('Player', playerSchema);
