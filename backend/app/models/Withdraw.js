'use strict'
/**
 * Module Dependencies
 */


const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const shortid = require('shortid');
const PlayerReward = mongoose.model('playersRewardHistory');
const wallet = mongoose.model('Wallet');
const RewardsMaster = mongoose.model('rewardMaster');

let withdrawSchema = new Schema({
    withdrawId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    // player selected bank detail for withdrawing money in
    bankDetailId: {
        type: String,
        default: ''
    },
    transactionId: {
        type: String,
        default: ''
    },
    playerId: {
        type: String,
        default: ''
    },
    // instant-paid,success,rejected,pending,approved
    status: {
        type: String,
        default: ''
    },
    amount: {
        type: Number,
        default: 0
    },
    milestoneId: {
        type: String,
        default: ''
    },
    productId: {
        type: String,
        default: ''
    },
    // new
    instantPay: {
        type: Boolean,
        default: false
    },
    // utr by cashfree
    utr: {
        type: String,
        default: ''
    },
    // referenceId by cashfree
    referenceId: {
        type: String,
        default: ''
    },
    verifiedBy: {
        type: String,
        default: ''
    },
    verifiedRemark: {
        type: String,
        default: ''
    },
    createdOn: {
        type: String,
        default: ""
    }


});

//Pre save for withdraw
withdrawSchema.pre('save', function (next) {
    const withdraw = this;
    mongoose.model('Withdraw').findOne({playerId: withdraw.playerId}, {}, {}, function (err, data) {
        if (data) {
            next();
        } else {
            console.log('no data');
            return Promise.resolve()
                .then(() => {
                    RewardsMaster.findOne({rewardName: 'Withdraw'})
                        .then((data) => {
                            return data;
                        })
                        .then((data) => {
                            let body = {}
                            body["playerRewardHistoryId"] = shortid.generate();
                            body["playerId"] = withdraw.playerId;
                            body["rewardMasterId"] = data.rewardMasterId;
                            body["rewardType"] = data.rewardType;
                            body["rewardAmount"] = data.rewardAmount;
                            body["createdOn"] = new Date();
                            return PlayerReward.create(body)
                                .then((res) => {
                                    data = {
                                        res: res,
                                        data: data
                                    }
                                    return data;
                                })
                        })
                        .then((data) => {
                            let json;
                            if (data.data.rewardType === 'cash') {
                                json = {
                                    $inc: {
                                        bonusCashBalance: data.data.rewardAmount,
                                        totalCashBalance: data.data.rewardAmount
                                    }
                                }
                            } else if (data.data.rewardType === 'token') {
                                json = {
                                    $inc: {
                                        totalTokenBalance: data.data.rewardAmount,
                                    }
                                }
                            }
                            wallet.findOneAndUpdate({playerId: withdraw.playerId}, json, {new: true})
                                .then((data) => {
                                    next()
                                })
                        })
                        .catch(err => {
                            console.log('err', err);
                            next()
                        })

                })
                .catch(err => {
                    console.log('err', err);
                    next()
                })
        }
    })
    // next();
})

mongoose.model('Withdraw', withdrawSchema);