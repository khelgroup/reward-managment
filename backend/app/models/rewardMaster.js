'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let rewardMasterSchema = new Schema({
    rewardMasterId: {
        type: String,
        default: '',
// enables us to search the record faster
        index: true,
        unique: true
    },
    // fieldName like username/ password/ avatar
    rewardName: {
        type: String,
        default: ''
    },// fieldName like username/ password/ avatar
    // rewardType like points/ coins/ ...
    rewardType: {
        type: String,
        default: 'token'
    },
    // rewardValue like 10 coins/ points/ etc
    rewardAmount: {
        type: Number,
        default: 100
    },
    // isActive for this rewards are not delete or not?
    isActive: {
        type: Boolean,
        default: true
    },
    // any description if needed
    description: {
        type: String,
        default: ''
    },
    // ID of admin if multiple admin available otherwise its not necessary
    createdOn: {
        type: Date,
        default: ''
    },
}, {versionKey: false});

mongoose.model('rewardMaster', rewardMasterSchema);
