const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const shortid = require('shortid');

let kycSchema = new Schema({
    kycId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    playerId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        // unique: true
    },
    //   aadhar/pan/election/passport
    documentType: {
        type: String,
        default: ''
    },
    documentNumber: {
        type: String,
        default: ''
    },
    documentProof: {
        type: String,
        default: ''
    },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    state: {
        type: String,
        default: ''
    },
    dateOfBirth: {
        type: Date,
        default: ''
    },
    imei: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: ''
    },
    productId: {
        type: String,
        default: ''
    },
    verifiedBy: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ""
    }
});

//Pre save for KYC
kycSchema.pre('save', function(next) {
    const PlayerReward = mongoose.model('playersRewardHistory');
    const wallet = mongoose.model('Wallet');
    const RewardsMaster = mongoose.model('rewardMaster');
    const kyc = this;
    mongoose.model('Kyc').findOne({playerId: kyc.playerId}, {}, {}, function (err, data) {
        if (data) {
            next();
        }
        else {
            console.log('no data');
            return Promise.resolve()
                .then(() => {
                    RewardsMaster.findOne({rewardName: 'Kyc'})
                        .then((data) => {
                            return data;
                        })
                        .then((data) => {
                            let body = {}
                            body["playerRewardHistoryId"] = shortid.generate();
                            body["playerId"] = kyc.playerId;
                            body["rewardMasterId"] = data.rewardMasterId;
                            body["rewardType"] = data.rewardType;
                            body["rewardAmount"] = data.rewardAmount;
                            body["createdOn"] = new Date();
                            return PlayerReward.create(body)
                                .then((res) => {
                                    data = {
                                        res: res,
                                        data: data
                                    }
                                    return data;
                                })
                        })
                        .then((data) => {
                            let json;
                            if (data.data.rewardType === 'cash') {
                                json = {
                                    $inc: {
                                        bonusCashBalance: data.data.rewardAmount,
                                        totalCashBalance: data.data.rewardAmount
                                    }
                                }
                            } else if (data.data.rewardType === 'token') {
                                json = {
                                    $inc: {
                                        totalTokenBalance: data.data.rewardAmount,
                                    }
                                }
                            }
                            wallet.findOneAndUpdate({playerId: kyc.playerId}, json, {new: true})
                                .then((data) => {
                                    next()
                                })
                        })
                        .catch(err => {
                            console.log('err', err);
                            next()
                        })

                })
                .catch(err => {
                    console.log('err', err);
                    next()
                })
        }
    })
    // next();
});

mongoose.model('Kyc', kycSchema);
