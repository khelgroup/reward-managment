'use strict'
/**
 * Module Dependencies
 */
const shortid = require('shortid');
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let depositSchema = new Schema({
    depositId: {
        type: String,
        default: '',
        // enables us to search the record faster
        index: true,
        unique: true
    },
    transactionId: {
        type: String,
        default: ''
    },
    playerId: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: ''
    },
    amount: {
        type: Number,
        default: 0
    },
    paymentMethod: {
        type: String,
        default: ''
    },
    paymentMethodType: {
        type: String,
        default: ''
    },
    paymentStatus: {
        type: String,
        default: ''
    },
    depositType: {
        type: String,
        default: ''
    },
    productId: {
        type: String,
        default: ''
    },
    createdOn: {
        type: Date,
        default: ""
    }


},{versionKey: false});

//Pre Save for Deposit
depositSchema.pre('save', function (next) {
    const PlayerReward = mongoose.model('playersRewardHistory');
    const wallet = mongoose.model('Wallet');
    const RewardsMaster = mongoose.model('rewardMaster');

    // do stuff
    const deposit = this;
    mongoose.model('Deposit').findOne({playerId: deposit.playerId}, {}, {}, function (err, data) {
        if (data) {
            next();
        }
        else {
            console.log('no data');
            return Promise.resolve()
                .then(() => {
                    RewardsMaster.findOne({rewardName: 'Deposit'})
                        .then((data) => {
                            return data;
                        })
                        .then((data) => {
                            let body = {}
                            body["playerRewardHistoryId"] = shortid.generate();
                            body["playerId"] = deposit.playerId;
                            body["rewardMasterId"] = data.rewardMasterId;
                            body["rewardType"] = data.rewardType;
                            body["rewardAmount"] = data.rewardAmount;
                            body["createdOn"] = new Date();
                            return PlayerReward.create(body)
                                .then((res) => {
                                    data = {
                                        res: res,
                                        data: data
                                    }
                                    return data;
                                })
                        })
                        .then((data) => {
                            let json;
                            if (data.data.rewardType === 'cash') {
                                json = {
                                    $inc: {
                                        bonusCashBalance: data.data.rewardAmount,
                                        totalCashBalance: data.data.rewardAmount
                                    }
                                }
                            } else if (data.data.rewardType === 'token') {
                                json = {
                                    $inc: {
                                        totalTokenBalance: data.data.rewardAmount,
                                    }
                                }
                            }
                            wallet.findOneAndUpdate({playerId: deposit.playerId}, json, {new: true})
                                .then((data) => {
                                    next()
                                })
                        })
                        .catch(err => {
                            console.log('err', err);
                            next()
                        })

                })
                .catch(err => {
                    console.log('err', err);
                    next()
                })
        }
    })
    // next();
});

mongoose.model('Deposit', depositSchema);